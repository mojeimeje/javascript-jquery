$(document).ready(function () {
    var form = $('#add_update_form');    
    var errorsArray = [10];
    var error;
    
    for (var i = 0; i <= 5; i++) {
        errorsArray[i] = 1;
    }
    
    $('#title_box').keyup(validateTitle);
    $('#textarea_contents').keyup(validateContents);
    $('#textarea_desc').keyup(validateDesc);
    $('#picture_upload').change(validateImage);
    
    
    function validateImage(){
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            errorsArray[2] = 0;
            if( !$('#picture_upload').val()) { 
                $('#pictureInfo').removeClass('valid');
                $('#pictureInfo').addClass('error');
                $('#pictureInfo').text('You have to upload a picture!');
                errorsArray[0] = 1;
            }
            else {
                var fsize = $('#picture_upload')[0].files[0].size; 
                
                if(fsize > 1048576)  {
                    $('#pictureInfo').removeClass('valid');
                    $('#pictureInfo').addClass('error');
                    $('#pictureInfo').text('Your picture is too big, please resize it!');
                    errorsArray[1] = 1;
                }
                
                else if (fsize <= 1048576) {
                    $('#pictureInfo').removeClass('error');
                    $('#pictureInfo').addClass('valid');
                    $('#pictureInfo').text('Your picture is valid!');
                    errorsArray[1] = 0;
                    errorsArray[0] = 0;
                }
            }    
        }
        else {
            $("#pictureInfo").html("Please upgrade your browser, because your current browser lacks some new features we need!");
            errorsArray[2] = 1;
        }
    }
    
 
    function validateTitle () {
        if ($('#title_box').val().length <= 20) {
           $('#titleInfo').removeClass('valid');
           $('#titleInfo').addClass('error');
           $('#titleInfo').text('Your title is too short!');
           errorsArray[3] = 1;
        }
        
        else {
            if ($('#title_box').val().length > 20) {
                var titleTitle = $('#title_box').val();
                $('#titleInfo').removeClass('error');
                $('#titleInfo').addClass('valid');
                $('#titleInfo').text('Title is valid!');
                errorsArray[3] = 0;
            }
        }
    }
    
    function validateContents () {
        if ($('#textarea_contents').val().length <= 500) {
           $('#contentsInfo').removeClass('valid');
           $('#contentsInfo').addClass('error');
           $('#contentsInfo').text('Your contents is too short!');
           errorsArray[4] = 1;
        }
        
        else {
            if ($('#textarea_contents').val().length > 500) {
                var cont = $('#textarea_contents').val();
                $('#contentsInfo').removeClass('error');
                $('#contentsInfo').addClass('valid');
                $('#contentsInfo').text('Contents is valid!');
                errorsArray[4] = 0;
            }
        }
    }
    
    function validateDesc () {
        if ($('#textarea_desc').val().length <= 100) {
           $('#descInfo').removeClass('valid');
           $('#descInfo').addClass('error');
           $('#descInfo').text('Your description is too short!');
           errorsArray[5] = 1;
        }
        
        else {
            if ($('#textarea_desc').val().length > 100) {
                var desc = $('#textarea_desc').val();
                $('#descInfo').removeClass('error');
                $('#descInfo').addClass('valid');
                $('#descInfo').text('Description is valid!');
                errorsArray[5] = 0;
            }
        }
    }
    
    $('form#add_update_form').submit(function () {
      //  var form_all = $('#add_update_form').serialize();
        var formData = new FormData($(this)[0]);
        var error;
        for (var i = 0; i <= 5; i++) {
            if (errorsArray[i] == 1) {
                error = 1;
                break;
            }
            else {
                error = 0;
            }
        }
        
        if(error == 0) {
            $('#formInfo').removeClass('error');
            $('#formInfo').addClass('valid');
            $('#formInfo').text('Alles gut!');
            
            $.ajax({
                type: 'POST',
                url: 'add_post_php.php',
                data: formData,
                async: false,
                //*******************SUCCESS*********
                success: function (response) {
                    event.preventDefault();
                    if (response != 0) {
                      window.location.href = "http://happytree.qsna.eu/post.php?id=" + response;
                    }
                     
                    else {
                        $('#formInfo').text('An error occured. Please try again later.'); 
                    }
                },
                    cache: false,
                    contentType: false,
                    processData: false,
                //var formData = new FormData($(this)[0]);
                //************************ERROR*********
                 error: function(jqXHR, textStatus, errorThrown) {
        	// Handle errors here
                alert('error');
        	console.log('ERRORS: ' + textStatus);
                },
                
                //**********************COMPLETE*********
               // complete: function() {
        	// STOP LOADING SPINNER
                //window.location.replace(window.location.pathname);
              //  }
            });
        }
        else if (error == 1) {
            event.preventDefault();
            $('#formInfo').removeClass('valid');
            $('#formInfo').addClass('error');
            $('#formInfo').text('Please check your errors!');
        } 
    });
}); 